# timeli-redisDB
The purpose of this application is to feed in necessary information into an instance of Redis Database to help [inrix-sparkstreaming](https://umesh_iastate@bitbucket.org/rbyna/timeli-sparkstreaming/src) in detecting potential incidents. It uploads the following data as keyvalue pairs into redis :

1. Threshold data 
2. Target Routes
3. Segment data
4. Segment and its neighbors with in 2 miles

Additional, information about the data stored can be found here [inrix-redis-data](https://docs.google.com/document/d/1ga7ZQJGntJydvXP_YrrddWORDEQoEAUkoExVzghH5M4/edit?ts=5cf96c67#bookmark=id.9vxxrf4xo275).


## Prerequisites
1. [Redis](https://redis.io/) 
    * version : 4.0.11
    * Configuration file location : `/etc/redis/redis.conf`
    * To start redis server : `sudo systemctl start redis`
    * To stop redis server : `sudo systemctl stop redis`
2. [Java](https://www.java.com/en/) 
     * version : 1.8
3. [Maven](https://maven.apache.org/)   

## Inputs
1. Threshold data  
     * type : file
     * format : csv
     * Update file location at `thresholdDataFileName` inside `LoadDataToRedis.java` 
2. Segment data 
      * type : file
      * format : csv
      * Update file location at `SegmentDataFileName` inside `LoadDataToRedis.java`
3. Old targets segment data 
      * type : file
      * format : csv
      * Update file location at `targetRoutesFileName` inside `LoadDataToRedis.java`
4. New targets segment data
       * type : file
       * format : csv
       * Update file location at `targetRoutesFileNameNew` inside `LoadDataToRedis.java`
       
Sample files of all the inputs are present in resource folder.

## Steps to run the application :
1. Make sure redis server is running
2. Use `maven` to build the jar `maven install` 
3. `java -jar timeli-redis.jar` Run it as simple java application

## Verification
To verify the data in redis, first connect to redis using redis-cli on the machine it is installed on. Based on the redis configuration, password maybe required to perform queries. 

1. Threshold Data : Use HGETALL to get the threshdata. For example,
     * `HGETALL 1485891183:2:6`  should return something like this  `1) "3.0" 2) "59.0" 3) "2.0" 4) "59.0" 5) "1.0" 6) "59.0" 7) "0.0"8) "55.0"`
2. Target Routes : Use HGET
      * `HGET 1485891183 code` should return true if the code is in the target routes file.  
3. Segment Data : Use HGETALL. For example,
      * `HGETALL 1485891183` should return all the information(fields) about the segment. 
      * `HGET 1485891183 roadlist` should return the road list of the segment.
4. Near by segments : Use lrange.
     *  `lrange 1485891183L 0 29` should return the first 29 near by segments of the segment code 1485891183.
   
         * `1) "1485890654" 2) "1485890639"3) "1485891242" 4) "1485891228" 5) "1485891211" 6) "1485891197" 7) "1485891167" 8) "1485890654" 9) "1485890639" 10) "1485891242" 11) "1485891228" 12) "1485891211" 13) "1485891197" 14) "1485891167"`
  
## Known Issues/Bugs
1. As can be seen in the result from near by segments example, the list contains duplicate values. The list repeats itself after number 7.
2. Logic of finding near by segments function needs to be verified. 
3. Current logic of the application doesn't provide the safety from uploading data into redis multiple times.
      * For example, running this application the second time without clearing the data in redis will add duplicates into the near by segments list. So, make sure to clear redis before running this application. 