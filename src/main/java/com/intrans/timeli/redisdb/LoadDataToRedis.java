/*
 * 
 */
package com.intrans.timeli.redisdb;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import redis.clients.jedis.GeoCoordinate;
import redis.clients.jedis.GeoRadiusResponse;
import redis.clients.jedis.GeoUnit;
import redis.clients.jedis.Jedis;

public class LoadDataToRedis {

	
	public static final String basePath = "/home/team/";
	// schema of the data : code weekday hour period median speed, iqd_speed
	public static final String thresholdDataFileName = basePath + "inrix_param_may31.csv";

	// schema : code, precode, start lat, start long, end lat, end long, distance(unit?), road
	public static final String SegmentDataFileName = basePath + "INRIX_XD_17_1.csv";
	public static final String targetRoutesFileName = basePath + "inrix-v2-freeways.csv";
	public static final String targetRoutesFileNameNew = basePath + "xd-interstate-feb9-2019.csv";

	/**
	 * Load threshold data.
	 * Loads threshold data as key value pair
	 * <p>Threshold speed = median - 3*iqd_speed</p>
	 * <p>For weekday, 0 means Sunday and 6 means Saturday. Similarly for period, 0
	 * means 0-14 mins, 1 means 15-29 mins, etc.</p>
	 * <p><code>:<weekday>:<period> will be the key and value be threshold</p>
	 * @param jedis the jedis connection instance
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void loadThresholdData(Jedis jedis) throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(thresholdDataFileName));

		String line = "";

		while ((line = br.readLine()) != null) {
			String[] parts = line.split(",");

			String code = parts[0];
			String weekday = parts[1];
			String hour = parts[2];
			String period = parts[3]; // periods are like 0.0, 1.0, 2.0, 3.0
			Double median = Double.parseDouble(parts[4]);
			Double iqd = Double.parseDouble(parts[5]);

			String key = code + ":" + weekday + ":" + hour;

			double threshold = median - (3 * iqd);

			jedis.hset(key, period, String.valueOf(threshold));
		}

	}

	/**
	 * Load Segment data.
	 * <p>Stores revelant segment details as code#fieldName#Value</p>
	 * @param jedis the jedis
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void loadSegmentData(Jedis jedis) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(SegmentDataFileName));

		// skip the header
		br.readLine();

		String line = null;

		while ((line = br.readLine()) != null) {
			// file is csv so split by comma (",")
			// https://stackoverflow.com/questions/1757065/java-splitting-a-comma-separated-string-but-ignoring-commas-in-quotes
			String[] splits = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
			String fid = splits[0];
			String oid_1 = splits[1];
			String code = splits[2];
			String previous_code = splits[3];
			String next_code = splits[4];
			String frc = splits[5];
			String roadNumber = splits[6];
			String roadname = splits[7];
			String linearid = splits[8];
			String country = splits[9];
			String state = splits[10];
			String county = splits[11];
			String district = splits[12];
			String miles = splits[13];
			String sliproad = splits[14];
			String specialroad = splits[15];
			String roadlist = splits[16];
			String startlat = splits[17];
			String startlong = splits[18];
			String endlat = splits[19];
			String endlong = splits[20];
			String bearing = splits[21];
			String xdgroup = splits[22];
			String shapesrid = splits[23];

			jedis.hset(code, "fid", fid);
			jedis.hset(code, "oid_1", oid_1);
			jedis.hset(code, "previous_code", previous_code);
			jedis.hset(code, "next_code", next_code);
			jedis.hset(code, "frc", frc);
			jedis.hset(code, "road_number", roadNumber);
			jedis.hset(code, "road", roadname);
			jedis.hset(code, "linearid", linearid);
			jedis.hset(code, "country", country);
			jedis.hset(code, "state", state);
			jedis.hset(code, "county", county);
			jedis.hset(code, "district", district);
			jedis.hset(code, "distance", miles);
			jedis.hset(code, "sliproad", sliproad);
			jedis.hset(code, "specialroad", specialroad);
			jedis.hset(code, "roadlist", roadlist);
			jedis.hset(code, "startlat", startlat);
			jedis.hset(code, "startlong", startlong);
			jedis.hset(code, "endlat", endlat);
			jedis.hset(code, "endlong", endlong);
			jedis.hset(code, "bearing", bearing);
			jedis.hset(code, "xdgroup", xdgroup);
			jedis.hset(code, "shapesrid", shapesrid);
		}

		br.close();
	}

	/**
	 * Load target routes.
	 * segemntcode#code#true - hash set with code as key+code and value is false
	 * @param jedis the jedis
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void loadTargetRoutes(Jedis jedis) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(targetRoutesFileName));

		// skip the header
		br.readLine();

		String line = null;

		while ((line = br.readLine()) != null) {
			jedis.hset(line.trim(), "code", "false");
		}
		br.close();
	}

	/**
	 * Load target routes new.
	 * segemntcode#code#true - hash set with code as key+code and value is true
	 * @param jedis the jedis
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void loadTargetRoutesNew(Jedis jedis) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(targetRoutesFileNameNew));

		// skip the header
		br.readLine();

		String line = null;

		while ((line = br.readLine()) != null) {
			String[] splits = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
			jedis.hset(splits[0].trim(), "code", "true");
		}
		br.close();
	}

	/**
	 * Load segment geo data.
	 * <p>For all the target routes, it finds the list of near by segments with the same bearing and adds it in redis.</p>
	 * <p>segemnt_code + L will be the key and value will be list of its neighbors</p>
	 * @param jedis the jedis
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void loadSegmentGeoData(Jedis jedis) throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(targetRoutesFileNameNew));

		// skip the header
		br.readLine();

		String line = null;

		HashMap<String, HashSet<String>> roadNameToCodeList = new HashMap<>();

		while ((line = br.readLine()) != null) {
//			String code = line.trim();
			String[] split = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
			String code = split[0].trim();
			// multiple roads might be seperated by ";"
			String[] roads = jedis.hget(code, "road").split(";");
			/*
			 * Creates a hashmap with road name as key and list of all segment codes it is associated with 
			 */
			for (String road : roads) {

				// road names can have more components other than letter and number
				// we are interested only in letter and number that are first two components
				String[] splits = road.split(" ");

				String roadName = splits[0] + splits[1];

				if (roadNameToCodeList.get(roadName) == null) {
					HashSet<String> codeList = new HashSet<>();
					codeList.add(code);
					roadNameToCodeList.put(roadName, codeList);
				} else {
					HashSet<String> codeList = roadNameToCodeList.get(roadName);
					codeList.add(code);
					roadNameToCodeList.put(roadName, codeList);
				}
			}
		}
		
		/*
		 * Adds geo data details into redis with road name as key and hashmap of code and its geo coordinates
		 */
		for (String roadname : roadNameToCodeList.keySet()) {

			HashSet<String> codeList = roadNameToCodeList.get(roadname);

//			System.out.println(roadname + " " + codeList);

			Map<String, GeoCoordinate> coordinateMap = new HashMap<String, GeoCoordinate>();
			for (String code : codeList) {

				double latitude = Double.parseDouble(jedis.hget(code, "startlat"));
				double longitude = Double.parseDouble(jedis.hget(code, "startlong"));
				coordinateMap.put(code, new GeoCoordinate(longitude, latitude));
			}
			jedis.geoadd(roadname, coordinateMap);
		}
		
		/*
		 * Adds neighbors of a segment within 2 miles using bearing. Key will be code+L and value will be 
		 * list of segment codes 
		 */
		for (String roadname : roadNameToCodeList.keySet()) {
			HashSet<String> codeList = roadNameToCodeList.get(roadname);

			for (String code : codeList) {
				double latitude = Double.parseDouble(jedis.hget(code, "startlat"));
				double longitude = Double.parseDouble(jedis.hget(code, "startlong"));
				String bearing = jedis.hget(code, "bearing");

				List<GeoRadiusResponse> members = jedis.georadius(roadname, longitude, latitude, 2, GeoUnit.MI);

				int size = members.size();

				for (int i = 0; i < size; i++) {
					String codeWithinDistance = members.get(i).getMemberByString();

					if (!codeWithinDistance.equals(code)) {
						String newbearing = jedis.hget(codeWithinDistance, "bearing");
						if (bearing.equals(newbearing)) {
							jedis.lpush(code + "L", codeWithinDistance);
						}
					}
				}
			}

		}
	}

}
