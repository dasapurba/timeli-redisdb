/*
 * 
 */
package com.intrans.timeli.redisdb;

import redis.clients.jedis.Jedis;

import java.io.IOException;

// TODO: Auto-generated Javadoc

public class Run {

    
    public static void main(String[] args) throws IOException {
        
    	LoadDataToRedis loaddb = new LoadDataToRedis();

        final Jedis jedis = initDb();
        jedis.auth("Clust3rax$");
        
        loaddb.loadThresholdData(jedis);
        loaddb.loadSegmentData(jedis);
        loaddb.loadTargetRoutes(jedis);
        loaddb.loadTargetRoutesNew(jedis);
        loaddb.loadSegmentGeoData(jedis);

    }

    /**
     * Inits the db.
     * Add host qnd port in constructor
     * Default values localhost, 6379
     * @see <a href="https://www.baeldung.com/jedis-java-redis-client-library">Jedis</a> is a java client for redis
     * @return the jedis instance
     */
    private static Jedis initDb(){
        return new Jedis("localhost");

    }
}
